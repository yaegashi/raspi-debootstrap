: ${PRODUCT=raspi}
: ${VERSION=}
: ${SUITE=buster}
: ${ARCH=armhf}
: ${MIRRORURL=http://archive.raspbian.org/raspbian}
: ${VARIANT=minbase}
# dpkg is a dummy to avoid empty argument
: ${INCLUDE=dpkg}
: ${EXCLUDE=nano}

BUILDDIR=$TOPDIR/build
BUILDKEYRING=$BUILDDIR/keyring
BUILDROOTFSDIR=$BUILDDIR/rootfs
BUILDARTIFACTSDIR=$BUILDDIR/artifacts
BASENAME=$PRODUCT-rootfs
ROOTFSTGZ=$BUILDARTIFACTSDIR/$BASENAME-$SUITE-$ARCH$VERSION.tar.gz
