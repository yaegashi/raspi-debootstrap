#!/bin/bash

set -eu

TOPDIR=$(cd $(dirname $0) && pwd)
cd $TOPDIR
. common.sh

apt-get update
apt-get install -y qemu-user-static debootstrap

mkdir -p $BUILDROOTFSDIR $BUILDARTIFACTSDIR

wget https://archive.raspbian.org/raspbian.public.key -q -O - | \
        gpg --no-default-keyring --keyring $BUILDKEYRING --import /dev/stdin

qemu-debootstrap \
        --arch "$ARCH" \
        --variant "$VARIANT" \
        --include "$INCLUDE" \
        --exclude "$EXCLUDE" \
        --keyring "$BUILDKEYRING" \
        $SUITE $BUILDROOTFSDIR $MIRRORURL

cat <<EOF >$BUILDROOTFSDIR/etc/apt/sources.list
deb $MIRRORURL $SUITE main contrib non-free rpi
deb http://archive.raspberrypi.org/debian $SUITE main ui
EOF
wget https://archive.raspberrypi.org/debian/raspberrypi.gpg.key -q -O $BUILDROOTFSDIR/key
chroot $BUILDROOTFSDIR apt-key add /key
chroot $BUILDROOTFSDIR apt-get update
rm -rf $BUILDROOTFSDIR/key $BUILDROOTFSDIR/var/lib/apt/*

tar --numeric-owner -C $BUILDROOTFSDIR -czvf $ROOTFSTGZ .
