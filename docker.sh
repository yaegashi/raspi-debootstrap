#!/bin/sh

set -eu

TOPDIR=$(cd $(dirname $0) && pwd)
cd $TOPDIR
. common.sh

zcat $ROOTFSTGZ | docker import - rootfs-base
docker tag rootfs-base $CI_REGISTRY_IMAGE/base
docker build -t $CI_REGISTRY_IMAGE/build .
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker push $CI_REGISTRY_IMAGE/base
docker push $CI_REGISTRY_IMAGE/build
