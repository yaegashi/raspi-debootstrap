# raspi-debootstrap

## Introduction

This project demonstrates bootstrapping the Linux/ARM userland
in a Docker container using GitLab CI.

It utilizes the following components:

- The Raspbian distribution to support ARMv6 based Raspberry Pi devices
- debootstrap: the standard bootstrapping utility for Debian
- qemu-user-static: the userland emulator for ARM executables

Each CI build publishes the following artifacts:

- A tar+gz archive which bundles Raspbian userland files
- Container images of Raspbian for further userland development

The QEMU userland emulation allows you to run ARM executables on AMD64 servers,
which could make the build much faster than on the target ARM devices.
And you can perform CI builds for free
by leveraging shared CI runners of *privileged* containers on GitLab.com.
